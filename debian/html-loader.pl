#!/usr/bin/perl -n

use strict;
use File::Spec;

chomp;
next if /^debian/;
my $orig = $_;
my($vol,$dir,$filename) = File::Spec->splitpath($orig);

open F, '<', $orig or die $!;
open F2, '>', '.perl-html-loader';

while(<F>) {
    if (m#<%= require\('html-loader!(.*?)'\) %>#) {
        local $/ = undef;
        my $partial = $1;
        print STDERR "Let's include $dir$partial into $orig\n";
        open F3, '<', "$dir$partial" or die $!;
        $_ = <F3>;
        close F3;
    }
    print F2 $_;
}
close F2;
close F;
my $save = $orig;
$save =~ s#/#%#g;
rename $orig, "debian/$save";
rename '.perl-html-loader', $orig;
