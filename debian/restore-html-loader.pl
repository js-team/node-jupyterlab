#!/usr/bin/perl -n

chomp;
next unless /%/;
my $src = $_;
my $dst = $_;
$dst =~ s#^debian/##;
$dst =~ s#%#/#g;
print STDERR "mv $src $dst\n";
rename $src, $dst;
